﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ShopLogic;
using Moq;
using ShopLogic.Interfaces;
using ShopLogic.HelperClasses;

namespace ShopTests
{
    [TestFixture]
    public class ShopTests
    {
        private static IAdmin _testAdmin;

        private static readonly IProductBase _testProductBase = new Mock<IProductBase>().Object;

        private static ICustomer[] _testCustomers;

        [SetUp]
        public void Setup()
        {
            const int customersQuantity = 5;
            _testCustomers = new ICustomer[customersQuantity];
            for (int i = 0; i < customersQuantity; i++)
            {
                var customerMock = new Mock<ICustomer>();
                customerMock.Setup(customer => customer.ID).Returns(i + 1);
                _testCustomers[i] = customerMock.Object;
            }
            var adminMock = new Mock<IAdmin>();
            _testAdmin = adminMock.Object;
        }

        private static IEnumerable<TestCaseData> GetAllOrdersTest_TestCases
        {
            get
            {
                var testCase = GetAllOrdersTest_GetTestCase();
                yield return new TestCaseData(testCase.customers, testCase.expected, testCase.admin);
                testCase = GetAllOrdersTest_GetTestCase();
                yield return new TestCaseData(new ICustomer[] { testCase.customers[2], testCase.customers[4], testCase.customers[0], testCase.customers[1], testCase.customers[3] }, testCase.expected, testCase.admin);
                testCase = GetAllOrdersTest_GetTestCase();
                yield return new TestCaseData(new ICustomer[] { testCase.customers[4], testCase.customers[3], testCase.customers[2], testCase.customers[1], testCase.customers[0] }, testCase.expected, testCase.admin);
            }
        }

        private static (ICustomer[] customers, IEnumerable<IOrder> expected, IAdmin admin) GetAllOrdersTest_GetTestCase()
        {
            const int length = 5;
            Mock<IOrder>[] orderMocks = new Mock<IOrder>[length];
            for (int i = 0; i < length; i++)
            {
                orderMocks[i] = new Mock<IOrder>();
                orderMocks[i].Setup(order => order.ID).Returns(i + 1);
            }
            var expectedList = new IOrder[length];
            for (int i = 0; i < length; i++)
                expectedList[i] = orderMocks[length - i - 1].Object;
            var expected = new CustomList<IOrder>(expectedList);
            Mock<ICustomer>[] customersMocks = new Mock<ICustomer>[length];
            for (int i = 0; i < length; i++)
                customersMocks[i] = new Mock<ICustomer>();
            var orders = new List<IOrder>[length];
            orders[0] = new List<IOrder> { orderMocks[0].Object, orderMocks[1].Object };
            orders[1] = new List<IOrder>();
            orders[2] = new List<IOrder>();
            orders[3] = new List<IOrder> { orderMocks[3].Object };
            orders[4] = new List<IOrder> { orderMocks[4].Object };
            var adminOrder = new List<IOrder> { orderMocks[2].Object };
            customersMocks[0].Setup(customer => customer.GetEnumerator()).Returns(orders[0].GetEnumerator());
            customersMocks[1].Setup(customer => customer.GetEnumerator()).Returns(orders[1].GetEnumerator());
            customersMocks[2].Setup(customer => customer.GetEnumerator()).Returns(orders[2].GetEnumerator());
            customersMocks[3].Setup(customer => customer.GetEnumerator()).Returns(orders[3].GetEnumerator());
            customersMocks[4].Setup(customer => customer.GetEnumerator()).Returns(orders[4].GetEnumerator());
            var adminMock = new Mock<IAdmin>();
            adminMock.Setup(admin => admin.GetEnumerator()).Returns(adminOrder.GetEnumerator());
            IAdmin testAdmin = adminMock.Object;
            return (new ICustomer[] { customersMocks[0].Object, customersMocks[1].Object, customersMocks[2].Object, customersMocks[3].Object, customersMocks[4].Object }, expected, testAdmin);
        }

        [Test]
        public void ConstructorTest_CorrectParameters_CreatesShopObject()
        {
            // Act
            Shop testShop = new Shop(_testAdmin, _testCustomers, _testProductBase);

            // Assert
            Assert.AreEqual(_testAdmin, testShop.Admin, "IAdmin objects should be equal");
            Assert.AreSame(_testAdmin, testShop.Admin, "IAdmin objects should have the same reference");
            for (int i = 0; i < _testCustomers.Length; i++)
            {
                Assert.AreEqual(_testCustomers[i], testShop.Customers[i], $"ICustomer objects should be equal. Problem at index {i}");
                Assert.AreSame(_testCustomers[i], testShop.Customers[i], $"ICustomer objects should have the same reference. Problem at index {i}");
            }
            Assert.AreEqual(_testProductBase, testShop.ShowCase, "IProductBase objects should be equal");
            Assert.AreSame(_testProductBase, testShop.ShowCase, "IProductBase objects should have the same reference");
        }

        [Test]
        public void ConstructorTest_NullAdmin_ThrowsArgumentNullException()
        {
            // Arrange
            IAdmin testAdmin = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => new Shop(testAdmin, _testCustomers, _testProductBase));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"The constructor should throw {expectedEx.GetType()} if the parameter admin is null");
        }

        [Test]
        public void ConstructorTest_NullCustomers_ThrowsArgumentNullException()
        {
            // Arrange
            IEnumerable<ICustomer> testCustomers = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => new Shop(_testAdmin, testCustomers, _testProductBase));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"The constructor should throw {expectedEx.GetType()} if the parameter customers is null");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void ConstructorTest_OneNullCustomer_ThrowsArgumentNullException(int index)
        {
            // Arrange
            ICustomer[] testCustomers = _testCustomers.Clone() as ICustomer[];
            testCustomers[index] = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => new Shop(_testAdmin, testCustomers, _testProductBase));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"The constructor should throw {expectedEx.GetType()} if any of customers is null");
        }

        [Test]
        public void ConstructorTest_NullProductBase_ThrowsArgumentNullException()
        {
            // Arrange
            IProductBase testProductBase = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => new Shop(_testAdmin, _testCustomers, testProductBase));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"The constructor should throw {expectedEx.GetType()} if the parameter productBase is null");
        }

        [Test]
        [TestCaseSource("GetAllOrdersTest_TestCases")]
        public void GetAllOrdersTest_ReturnsSortedByID(IEnumerable<ICustomer> testCustomers, CustomList<IOrder> expected, IAdmin admin)
        {
            // Arrange
            Shop testShop = new Shop(admin, testCustomers, _testProductBase);

            // Act
            var actual = testShop.GetAllOrders();

            // Assert
            Assert.AreEqual(expected, actual, "Method GetAllOrders does not returns all orders at right positions");
        }

        [Test]
        public void IsFreeLoginTest_Free_ReturnsTrue()
        {
            // Arrange
            const string login = "temp";
            Shop testShop = new Shop(_testAdmin, _testCustomers, _testProductBase);

            // Act
            var actual = testShop.IsFreeLogin(login);

            // Assert
            Assert.IsTrue(actual, "Method IsFreeLogin should return true if the login is free");
        }

        [Test]
        public void IsFreeLoginTest_NotFreeAdminCollision_ReturnsFalse()
        {
            // Arrange
            const string login = "temp";
            var adminMock = new Mock<IAdmin>();
            adminMock.Setup(admin => admin.CheckLogin(It.IsAny<string>())).Returns(true);
            adminMock.Setup(admin => admin.Login).Returns(login);
            _testAdmin = adminMock.Object;
            Shop testShop = new Shop(_testAdmin, _testCustomers, _testProductBase);

            // Act
            var actual = testShop.IsFreeLogin(login);

            // Assert
            Assert.IsFalse(actual, "Method IsFreeLogin should return false if the login is not free (collision with admin)");
        }

        [Test]
        public void IsFreeLoginTest_NotFreeCustomerCollision_ReturnsFalse()
        {
            // Arrange
            const string login = "temp";
            const int customersQuantity = 5;
            _testCustomers = new ICustomer[customersQuantity];
            for (int i = 0; i < customersQuantity; i++)
            {
                var customerMock = new Mock<ICustomer>();
                customerMock.Setup(customer => customer.ID).Returns(i + 1);
                if (i == 3)
                    customerMock.Setup(customer => customer.CheckLogin(It.IsAny<string>())).Returns(true);
                _testCustomers[i] = customerMock.Object;
            }
            Shop testShop = new Shop(_testAdmin, _testCustomers, _testProductBase);

            // Act
            var actual = testShop.IsFreeLogin(login);

            // Assert
            Assert.IsFalse(actual, "Method IsFreeLogin should return false if the login is not free (collision with customer)");
        }

        [Test]
        [TestCase(null)]
        public void IsFreeLoginTest_NullLogin_ThrowsArgumentNullException(string login)
        {
            // Arrange
            Shop testShop = new Shop(_testAdmin, _testCustomers, _testProductBase);
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => testShop.IsFreeLogin(login));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method IsFreeLogin should throw {expectedEx.GetType()} if the login is null");
        }
    }
}
