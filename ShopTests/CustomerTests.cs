﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ShopLogic;
using ShopLogic.Interfaces;
using Moq;
using ShopLogic.HelperClasses;

namespace ShopTests
{
    [TestFixture]
    public class CustomerTests
    {

        private static readonly string _testPassword = "CustPass1";

        private static Customer _testCustomer;

        private static INotifier _testNotifier;

        [SetUp]
        public void Setup()
        {
            _testNotifier = new Mock<INotifier>().Object;
            _testCustomer = new Customer("Petro", "Petrov", new DateTime(2001,1,1), "petro@gmail.com", "+380682777777", _testPassword, _testNotifier);
        }

        [Test]
        [TestCase("Admin1Psw")]
        [TestCase("Admin1psw")]
        [TestCase("admin1Psw")]
        [TestCase("A1111111w")]
        [TestCase("AdMIN1PSW")]
        [TestCase("adsa^%*#!@e802q3uASWJA;sajO213@#@$2")]
        public void PasswordSetterTest_ValidLogin_SetsPassword(string password)
        {
            // Act
            _testCustomer.Password = password;

            // Assert
            Assert.IsTrue(_testCustomer.CheckPassword(password), "Property does not sets password");
        }

        [Test]
        [TestCase("Psword1")]
        [TestCase("password")]
        [TestCase("Password")]
        [TestCase("123312432")]
        [TestCase("123312432pass")]
        [TestCase("123312432PASS")]
        [TestCase("")]
        public void PasswordSetterTest_NotValidPassword_ThrowsFormatException(string password)
        {
            // Arrange
            var expectedEx = typeof(System.FormatException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.Password = password);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property password's setter should throw {expectedEx.GetType()} if given password contains white spaces, it length is smaller than 8 or it does not contain at least one uppercase letter, one lowercase letter and one digit");
        }

        [Test]
        [TestCase(null)]
        public void PasswordSetterTest_NullPassword_ThrowsArgumentNullException(string password)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.Password = password);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property password's setter should throw {expectedEx.GetType()} if given password is null or empty");
        }

        [Test]
        [TestCase("380682777777")]
        [TestCase("+380682777777")]
        [TestCase("+38-068-277-77-77")]
        [TestCase("+38 068 277 77 77")]
        [TestCase("+38 068 277-77-77")]
        [TestCase("+38 068 277-77-77")]
        [TestCase("+(38 068) 277-77-77")]
        public void CheckLoginTest_RealPhoneNumber_ReturnsTrue(string phoneNumber)
        {
            // Act
            var actual = _testCustomer.CheckLogin(phoneNumber);

            // Assert
            Assert.IsTrue(actual, "Method CheckLogin does not checks the login");
        }

        [Test]
        public void CheckLoginTest_RealEmail_ReturnsTrue()
        {
            // Arrange
            var login = _testCustomer.Email;

            // Act
            var actual = _testCustomer.CheckLogin(login);

            // Assert
            Assert.IsTrue(actual, "Method CheckLogin does not checks the login");
        }

        [Test]
        public void CheckLoginTest_NotALogin_ReturnsFalse()
        {
            // Arrange
            var login = "Not a login";

            // Act
            var actual = _testCustomer.CheckLogin(login);

            // Assert
            Assert.IsFalse(actual, "Method CheckLogin does not checks the login");
        }

        [Test]
        public void CheckLoginTest_NullLogin_ThrowsArgumentNullException()
        {
            // Arrange
            string login = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.CheckLogin(login));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method CheckLogin should throw {expectedEx.GetType()} if the potential login (passed parameter) is null");
        }

        [Test]
        public void CheckPasswordTest_RealPassword_ReturnsTrue()
        {
            // Arrange
            var login = _testPassword;

            // Act
            var actual = _testCustomer.CheckPassword(login);

            // Assert
            Assert.IsTrue(actual, "Method CheckPassword does not checks the password");
        }

        [Test]
        public void CheckPasswordTest_NotAPassword_ReturnsFalse()
        {
            // Arrange
            var password = "NotAPass1";

            // Act
            var actual = _testCustomer.CheckPassword(password);

            // Assert
            Assert.IsFalse(actual, "Method CheckPassword does not checks the password");
        }

        [Test]
        public void CheckLoginTest_NullPassword_ThrowsArgumentNullException()
        {
            // Arrange
            string password = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.CheckPassword(password));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method CheckPassword should throw {expectedEx.GetType()} if the potential password (passed parameter) is null");
        }

        [Test]
        [TestCase(1)]
        [TestCase(5)]
        public void AddOrderTest_CorrectOrder_ReturnsOrderWithTheSameReference(int quantity)
        {
            // Arrange
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(_testCustomer.ID);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
            var testOrder = orderMock.Object;

            // Act
            for (int i = 0; i < quantity; i++)
                _testCustomer.AddOrder(testOrder);

            // Assert
            Assert.AreEqual(quantity, _testCustomer.OrdersQuantity, "Method Add does not changes the quantity of orders");
            for (int i = 0; i < quantity; i++)
            {
                Assert.AreEqual(testOrder, _testCustomer[i], "Method Add did not add right order");
                Assert.AreSame(testOrder, _testCustomer[i], "Method Add did not add order with the same reference");
            }
        }

        [Test]
        public void AddOrderTest_AnotherCustomerID_ThrowsArgumentException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentException);
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(-1);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
            var testOrder = orderMock.Object;

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.AddOrder(testOrder));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method AddOrder should throw {expectedEx.GetType()} if the order has another customer's id");
        }

        [Test]
        public void AddOrderTest_NotConfirmed_ThrowsArgumentException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentException);
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(_testCustomer.ID);
            orderMock.Setup(order => order.IsConfirmed).Returns(false);
            var testOrder = orderMock.Object;

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.AddOrder(testOrder));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method AddOrder should throw {expectedEx.GetType()} if the order is not confirmed");
        }

        [Test]
        public void AddOrderTest_NullOrder_ThrowsArgumentNullException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);
            IOrder testOrder = null;

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.AddOrder(testOrder));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method AddOrder should throw {expectedEx.GetType()} if the order is null");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void IndexerTest_ReturnsIOrderWithSameRef(int index)
        {
            // Arrange
            var expectedList = new List<IOrder>();
            const int quantity = 5;
            for (int i = 0; i < quantity; i++)
            {
                var orderMock = new Mock<IOrder>();
                orderMock.Setup(order => order.Customer.ID).Returns(_testCustomer.ID);
                orderMock.Setup(order => order.IsConfirmed).Returns(true);
                orderMock.Setup(order => order.ID).Returns(i + 1);
                var testOrder = orderMock.Object;
                expectedList.Add(testOrder);
                _testCustomer.AddOrder(testOrder);
            };
            var expected = expectedList[index];

            // Act
            var actual = _testCustomer[index];

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Indexer does not return right value");
            Assert.AreSame(expected, actual,
                            message: "Order's references should be the same");
        }

        [Test]
        [TestCase(-3)]
        [TestCase(-1)]
        [TestCase(5)]
        [TestCase(10)]
        public void IndexerTest_ThrowsArgumentOutOfRangeException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            IOrder func() => _testCustomer[index];
            Exception actualEx = Assert.Catch(() => func());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Indexer should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        public void EnumeratorTest_ReturnsIOrderWithTheSameReference()
        {
            // Arrange
            var expectedList = new List<IOrder>();
            const int quantity = 5;
            for (int i = 0; i < quantity; i++)
            {
                var orderMock = new Mock<IOrder>();
                orderMock.Setup(order => order.Customer.ID).Returns(_testCustomer.ID);
                orderMock.Setup(order => order.IsConfirmed).Returns(true);
                orderMock.Setup(order => order.ID).Returns(i + 1);
                var testOrder = orderMock.Object;
                expectedList.Add(testOrder);
                _testCustomer.AddOrder(testOrder);
            };
            var actualList = new List<IOrder>();

            // Act
            foreach (var item in _testCustomer)
                actualList.Add(item);

            // Assert
            for (int i = 0; i < _testCustomer.OrdersQuantity; i++)
            {
                Assert.AreEqual(expectedList[i], actualList[i], "Enumerator returns incorrect value");
                Assert.AreSame(expectedList[i], actualList[i], "Enumerator should return the order with the same reference");
            }
        }

        [Test]
        [TestCase(0)]
        [TestCase(5)]
        public void StatusSetHandlerTest_CorrectParameters_AddsNotification(int expectedSize)
        {
            // Arrange
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(_testCustomer.ID);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
            var testOrder = orderMock.Object;
            var obj = new object();
            var args = new StatusEventArgs(testOrder, OrderStatuses.New, OrderStatuses.PaymentReceived);

            // Act
            for (int i = 0; i < expectedSize; i++)
                _testCustomer.StatusSetHandler(obj, args);

            // Assert
            Assert.AreEqual(expectedSize, _testCustomer.UnreadNotifications.Count, "Method StatusSetHandler does not adds the notification");
            for (int i = 0; i < expectedSize; i++)
            {
                Assert.AreEqual(obj, _testCustomer.UnreadNotifications[i].sender, "Method StatusSetHandler does not sets equal senders");
                Assert.AreEqual(args, _testCustomer.UnreadNotifications[i].eventArgs, "Method StatusSetHandler does not sets equal event args");
            }
        }

        [Test]
        [TestCase(0)]
        [TestCase(5)]
        public void StatusSetHandlerTest_AnotherCustomerID_DoesNotAddNotification(int size)
        {
            // Arrange
            const int notACustomerID = -1;
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(notACustomerID);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
            var testOrder = orderMock.Object;
            var obj = new object();
            var args = new StatusEventArgs(testOrder, OrderStatuses.New, OrderStatuses.PaymentReceived);
            const int expectedSize = 0;

            // Act
            for (int i = 0; i < size; i++)
                _testCustomer.StatusSetHandler(obj, args);

            // Assert
            Assert.AreEqual(expectedSize, _testCustomer.UnreadNotifications.Count, "Method StatusSetHandler adds the notification despite the order's customer has another ID");
        }

        [Test]
        public void StatusSetHandlerTest_NullSender_ThrowsArgumentNullException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(_testCustomer.ID);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
            var testOrder = orderMock.Object;
            object obj = null;
            var args = new StatusEventArgs(testOrder, OrderStatuses.New, OrderStatuses.PaymentReceived);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.StatusSetHandler(obj, args));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method StatusSetHandler should throw {expectedEx.GetType()} if the sender is null");
        }

        [Test]
        public void StatusSetHandlerTest_NullArgs_ThrowsArgumentNullException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);
            object obj = new object();
            StatusEventArgs args = null;

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.StatusSetHandler(obj, args));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method StatusSetHandler should throw {expectedEx.GetType()} if the StatusEventArgs e is null");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(5)]
        public void ClearNotificationsTest(int quantity)
        {
            // Arrange
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(_testCustomer.ID);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
            var testOrder = orderMock.Object;
            var obj = new object();
            var args = new StatusEventArgs(testOrder, OrderStatuses.New, OrderStatuses.PaymentReceived);
            for (int i = 0; i < quantity; i++)
                _testCustomer.StatusSetHandler(obj, args);
            const int expectedSize = 0;

            // Act
            _testCustomer.ClearNotifications();

            // Assert
            Assert.AreEqual(expectedSize, _testCustomer.UnreadNotifications.Count, "Method Clear does not clears the notifications list");
        }

        [Test]
        [TestCase("Nikita")]
        [TestCase("Tim")]
        [TestCase("John")]
        [TestCase("Vladislav")]
        [TestCase("Li")]
        public void FirstNameSetterTest_ValidName_SetsFirstName(string expected)
        {
            // Act
            _testCustomer.FirstName = expected;
            var actual = _testCustomer.FirstName;

            // Assert
            Assert.AreEqual(expected, actual, "Property's FirstName setter should set the first name if the passed parameter is valid");
        }

        [Test]
        [TestCase("nikita")]
        [TestCase("T")]
        [TestCase("y")]
        [TestCase("   Vladislav")]
        [TestCase("Stani$lav")]
        [TestCase("L1")]
        [TestCase("")]
        [TestCase("     ")]
        public void FirstNameSetterTest_NotValidName_ThrowsFormatException(string potentialName)
        {
            // Arrange
            var expectedEx = typeof(System.FormatException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.FirstName = potentialName);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's FirstName setter should throw {expectedEx.GetType()} if the passed parameter is not valid");
        }

        [Test]
        [TestCase(null)]
        public void FirstNameSetterTest_NullName_ThrowsArgumentNullException(string potentialName)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.FirstName = potentialName);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's FirstName setter should throw {expectedEx.GetType()} if the passed parameter is null");
        }

        [Test]
        [TestCase("Cooper")]
        [TestCase("Ostrohradsky")]
        [TestCase("Berners-Lee")]
        [TestCase("Li")]
        public void LastNameSetterTest_ValidName_SetsLastName(string expected)
        {
            // Act
            _testCustomer.LastName = expected;
            var actual = _testCustomer.LastName;

            // Assert
            Assert.AreEqual(expected, actual, "Property's LastName setter should set the last name if the passed parameter is valid");
        }

        [Test]
        [TestCase("T")]
        [TestCase("y")]
        [TestCase("L1")]
        [TestCase("")]
        [TestCase("     ")]
        [TestCase("Co0per")]
        [TestCase("$idorov")]
        [TestCase("So%enkov")]
        public void LastNameSetterTest_NotValidName_ThrowsFormatException(string potentialName)
        {
            // Arrange
            var expectedEx = typeof(System.FormatException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.LastName = potentialName);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's LastName setter should throw {expectedEx.GetType()} if the passed parameter is not valid");
        }

        [Test]
        [TestCase(null)]
        public void LastNameSetterTest_NullName_ThrowsArgumentNullException(string potentialName)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.LastName = potentialName);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's LastName setter should throw {expectedEx.GetType()} if the passed parameter is null");
        }

        [Test]
        [TestCase("4nick@gmail.com")]
        [TestCase("nick@hotmail.com")]
        [TestCase("423@ukr.net")]
        [TestCase("4$3@ukr.net")]
        [TestCase("4$3@ukr.kyiv.ua")]
        [TestCase("temp1.te@yandex.ru")]
        [TestCase(@"Fred\ Bloggs @example.com")]
        [TestCase(@"Joe.\\Blow @example.com")]
        [TestCase("\"Fred Bloggs\"@example.com")]
        [TestCase(@"customer/department= shipping@example.com")]
        [TestCase(@"$A12345 @example.com")]
        [TestCase(@"!def!xyz%abc @example.com")]
        [TestCase(@"_somename@example.com")]
        public void EmailSetterTest_ValidEmail_SetsEmail(string expected)
        {
            // Act
            _testCustomer.Email = expected;
            var actual = _testCustomer.Email;

            // Assert
            Assert.AreEqual(expected, actual, "Property's Email setter should set the email if the passed parameter is valid");
        }

        [Test]
        [TestCase("nikita")]
        [TestCase("temp1@")]
        [TestCase("temp1@.")]
        [TestCase("temp1@eee.")]
        [TestCase("temp1.")]
        [TestCase("")]
        [TestCase("     ")]
        public void EmailSetterTest_NotValidEmail_ThrowsFormatException(string potentialEmail)
        {
            // Arrange
            var expectedEx = typeof(System.FormatException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.Email = potentialEmail);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's Email setter should throw {expectedEx.GetType()} if the passed parameter is not valid");
        }

        [Test]
        [TestCase(null)]
        public void EmailSetterTest_NullEmail_ThrowsArgumentNullException(string potentialEmail)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.Email = potentialEmail);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's Email setter should throw {expectedEx.GetType()} if the passed parameter is null");
        }

        [Test]
        [TestCase("380682777777")]
        [TestCase("+380682777777")]
        [TestCase("+38-068-277-77-77")]
        [TestCase("+38 068 277 77 77")]
        [TestCase("+38 068 277-77-77")]
        [TestCase("+38 068 277-77-77")]
        [TestCase("+(38 068) 277-77-77")]
        [TestCase("+71234567890")]
        [TestCase("+15852826539")]
        [TestCase("+3584573975697")]
        public void PhoneNumberSetterTest_ValidPhoneNumber_SetsPhoneNumber(string expected)
        {
            // Act
            _testCustomer.PhoneNumber = expected;
            var actual = _testCustomer.PhoneNumber;

            // Assert
            Assert.AreEqual(expected.NormalizePhoneNumber(), actual, "Property's PhoneNumber setter should set the phone number if the passed parameter is valid");
        }

        [Test]
        [TestCase("n")]
        [TestCase("hello")]
        [TestCase("+++380682777777")]
        [TestCase("+---------")]
        [TestCase("---------")]
        [TestCase("+380++683777777")]
        [TestCase("+380n82777777")]
        [TestCase("+380#82777777")]
        [TestCase("+3806n82777777")]
        [TestCase("+3806$82777777")]
        [TestCase("+000000000000")]
        [TestCase("000000000000")]
        [TestCase("555")]
        [TestCase("911")]
        [TestCase("     ")]
        [TestCase("")]
        public void PhoneNumberSetterTest_NotValidPhoneNumber_ThrowsFormatException(string potentialPhoneNumber)
        {
            // Arrange
            var expectedEx = typeof(System.FormatException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.PhoneNumber = potentialPhoneNumber);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's PhoneNumber setter should throw {expectedEx.GetType()} if the passed parameter is not valid");
        }

        [Test]
        [TestCase(null)]
        public void PhoneNumberSetterTest_NullPhoneNumber_ThrowsArgumentNullException(string potentialPhoneNumber)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.PhoneNumber = potentialPhoneNumber);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's PhoneNumber setter should throw {expectedEx.GetType()} if the passed parameter is null");
        }

        [Test]
        [TestCase(1870, 1, 1)]
        [TestCase(1900, 1, 1)]
        [TestCase(1950, 12, 28)]
        [TestCase(1990, 2, 3)]
        [TestCase(2000, 4, 2)]
        [TestCase(2003, 1, 1)]
        [TestCase(2010, 10, 10)]
        public void BirthdaySetterTest_ValidBirthday_SetsBirthday(int year, int month, int day)
        {
            // Arrange
            var expected = new DateTime(year, month, day);

            // Act
            _testCustomer.Birthday = expected;
            var actual = _testCustomer.Birthday;

            // Assert
            Assert.AreEqual(expected, actual, "Property's Birthday setter should set the birthday if the passed parameter is valid");
        }

        [Test]
        [TestCase(1, 1, 1)]
        [TestCase(900, 12, 28)]
        [TestCase(1000, 2, 3)]
        [TestCase(1250, 12, 1)]
        [TestCase(1500, 4, 2)]
        [TestCase(1800, 1, 1)]
        [TestCase(1869, 10, 10)]
        public void BirthdaySetterTest_NotValidBirthday_ThrowsArgumentOutOfRangeException(int year, int month, int day)
        {
            // Arrange
            var birthday = new DateTime(year, month, day);
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.Birthday = birthday);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's Birthday setter should throw {expectedEx.GetType()} if the passed parameter is not valid");
        }

        [Test]
        public void BirthdaySetterTest_NotValidBirthdayMoreThanNow_ThrowsArgumentOutOfRangeException()
        {
            // Arrange
            var birthday = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day + 1);
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomer.Birthday = birthday);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property's Birthday setter should throw {expectedEx.GetType()} if the passed parameter is not valid");
        }
    }
}