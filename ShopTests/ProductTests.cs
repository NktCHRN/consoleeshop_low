using System;
using System.Collections.Generic;
using NUnit.Framework;
using ShopLogic;

namespace ShopTests
{
    [TestFixture]
    public class ProductTests
    {

        private Product _testProduct;

        [SetUp]
        public void Setup()
        {
            _testProduct = new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.5m);
        }

        private static IEnumerable<TestCaseData> EqualsTestCases
        {
            get
            {
                yield return new TestCaseData(new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.5m), true);
                yield return new TestCaseData(new Product("Cabbage", Categories.SportEquipment, "Leafy cabbage from Ukraine", 77.5m), false);
                yield return new TestCaseData(new Product("Green cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.5m), false);
                yield return new TestCaseData(new Product("Cabbage", Categories.Food, "Leafy cabbage from Russia", 77.5m), false);
                yield return new TestCaseData(new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77m), false);
                yield return new TestCaseData(null, false);
                yield return new TestCaseData(new object(), false);
            }
        }

        [Test]
        [TestCase("Green cabbage")]
        [TestCase("Red cabbage")]
        public void NameSetterTest_CorrectName(string name)
        {
            // Act
            _testProduct.Name = name;

            // Assert
            Assert.AreEqual(name, _testProduct.Name, message: "Property do not sets name");
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("         ")]
        public void NameSetterTest_IncorrectName_ThrowsArgumentNullException(string name)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testProduct.Name = name);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property should throw {expectedEx.GetType()} if name is null, empty string or contains only white spaces");
        }

        [Test]
        [TestCase("Green cabbage from Russia")]
        [TestCase("Red cabbage from Belarus")]
        public void DescriptionSetterTest_CorrectDescription(string description)
        {
            // Act
            _testProduct.Description = description;

            // Assert
            Assert.AreEqual(description, _testProduct.Description, message: "Property do not sets description");
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("         ")]
        public void DescriptionSetterTest_IncorrectDescription_ThrowsArgumentNullException(string description)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testProduct.Description = description);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property should throw {expectedEx.GetType()} if decsription is null, empty string or contains only white spaces");
        }

        [Test]
        [TestCase(77.5)]
        [TestCase(77.57)]
        [TestCase(77.575)]
        [TestCase(0.01)]
        [TestCase(1000)]
        public void PriceSetterTest_CorrectPrice(decimal price)
        {
            // Arrange
            decimal tolerance = 0.01m;

            // Act
            _testProduct.Price = price;

            // Assert
            Assert.That(_testProduct.Price, Is.EqualTo(price).Within(tolerance), message: "Property do not sets price");
        }

        [Test]
        [TestCase(-77.5)]
        [TestCase(-0.01)]
        [TestCase(0)]
        public void PriceSetterTest_IncorrectPrice_ThrowsArgumentOutOfRangeException(decimal price)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testProduct.Price = price);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property should throw {expectedEx.GetType()} if price is <= 0");
        }

        [Test]
        public void Clone_ReturnsDeepCopy()
        {
            // Act
            Product deepCopy = (Product)_testProduct.Clone();

            // Assert
            Assert.AreEqual(_testProduct, deepCopy,
                            message: "Method Clone() does not copy the object");
            Assert.AreNotSame(_testProduct, deepCopy,
                            message: "Method Clone() does not return deepcopy");
        }

        [Test]
        [TestCaseSource("EqualsTestCases")]
        public void Equals_ReturnsRightResult(object other, bool expected)
        {
            // Act
            bool actual = _testProduct.Equals(other);

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Method Equals does not work properly");
        }
    }
}
