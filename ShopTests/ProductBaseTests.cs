﻿using System;
using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;
using ShopLogic;
using ShopLogic.Interfaces;

namespace ShopTests
{
    [TestFixture]
    class ProductBaseTests
    {

        private (IProduct, int)[] _testArray;

        private ProductBase _testBase;

        [SetUp]
        public void Setup()
        {
            (IProduct, int)[] array = { (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
                (new Product("Barbell", Categories.SportEquipment, "Metal vulture for a barbell", 350m), 8),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2),
            (new Product("Pen", Categories.Stationery, "A blue ball point pen", 15m), 0),
            (new Product("Knife", Categories.Appliances, "A luxorious multifunctional swiss knife", 350m), 3)};
            _testArray = array;
            _testBase = new ProductBase(array);
        }

        private static IEnumerable<TestCaseData> FindTest_InBase_TestCases
        {
            get
            {
                yield return new TestCaseData(new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m));
                yield return new TestCaseData(new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m));
                yield return new TestCaseData(new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m));
                yield return new TestCaseData(new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m));
            }
        }

        private static IEnumerable<TestCaseData> FindTest_NotInBase_TestCases
        {
            get
            {
                yield return new TestCaseData(null);
                yield return new TestCaseData(new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m));
            }
        }

        private static IEnumerable<TestCaseData> AddTest_CorrectProduct_TestCases
        {
            get
            {
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 1));
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 5));
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 0));
                yield return new TestCaseData(new ProductBase(), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 10));
                yield return new TestCaseData(new ProductBase(), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 1));
                yield return new TestCaseData(new ProductBase(), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 0));
            }
        }


        private static IEnumerable<TestCaseData> AddTest_ExistingProduct_TestCases
        {
            get
            {
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 1));
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 10));
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 3));
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 0));
            }
        }

        private static IEnumerable<TestCaseData> AddTest_NullProduct_TestCases
        {
            get
            {
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), ((Product)null, 1));
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), ((Product)null, 5));
                yield return new TestCaseData(new ProductBase(), ((Product)null, 10));
                yield return new TestCaseData(new ProductBase(), ((Product)null, 1));
                yield return new TestCaseData(new ProductBase(), ((Product)null, 0));
            }
        }

        private static IEnumerable<TestCaseData> AddTest_IncorrectQuantity_TestCases
        {
            get
            {
                yield return new TestCaseData(new ProductBase(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), -1));
                yield return new TestCaseData(new ProductBase(), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), -5));
            }
        }

        [Test]
        [TestCase(null)]
        public void IEnumerableConstructorTest_ThrowsAgrumentNullException(IEnumerable<(IProduct, int)> products)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => new ProductBase(products));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Constructor should throw {expectedEx.GetType()} if IEnumerable products is null");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(6)]
        [TestCase(7)]
        public void IndexerTest_ReturnsTupleWithSameProductRef(int index)
        {
            // Arrange
            var expected = _testArray[index];

            // Act
            var actual = _testBase[index];

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Indexer does not return right value");
            Assert.AreSame(expected.Item1, actual.product,
                            message: "Product's references should be the same");
        }

        [Test]
        [TestCase(-3)]
        [TestCase(-1)]
        [TestCase(8)]
        [TestCase(10)]
        public void IndexerTest_ThrowsArgumentOutOfRangeException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            (IProduct, int) func() => _testBase[index];
            Exception actualEx = Assert.Catch(() => func());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Indexer should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(0, 1)]
        [TestCase(1, 10)]
        [TestCase(7, 5)]
        [TestCase(2, 0)]
        public void SetQuantityAtTest_CorrectParameters_SetsQuantityAtIndex(int index, int quantity)
        {
            // Arrange
            var expected = quantity;

            // Act
            _testBase.SetQuantityAt(index, quantity);
            var actual = _testBase[index].quantity;

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Method SetQuantity works incorrectly");
        }

        [Test]
        [TestCase(-1, 1)]
        [TestCase(8, 1)]
        public void SetQuantityAtTest_IncorrectIndex_ThrowsArgumentOutOfRangeException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testBase.SetQuantityAt(index, quantity));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method SetQuantity should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(1, -10)]
        [TestCase(1, -1)]
        public void SetQuantityAtTest_IncorrectQuantity_ThrowsArgumentOutOfRangeException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testBase.SetQuantityAt(index, quantity));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method SetQuantity should throw {expectedEx.GetType()} if quantity is negative or equal zero");
        }

        [Test]
        [TestCase(0, 5)]
        [TestCase(2, 3)]
        [TestCase(1, 10)]
        [TestCase(7, 1)]
        public void SellTest_CorrectParameters_SetsQuantityAtIndex(int index, int quantity)
        {
            // Arrange
            var expected = (_testArray[index].Item1, quantity);
            var expectedQuantity = _testArray[index].Item2 - quantity;

            // Act

            var actual = _testBase.Sell(index, quantity);

            // Assert
            Assert.AreEqual(expected.Item1, actual.product,
                            message: "Method Sell returns wrong product");
            Assert.AreEqual(expected.quantity, actual.quantity,
                message: "Method Sell returns wrong quantity");
            Assert.AreEqual(expectedQuantity, _testBase[index].quantity,
                message: "Method Sell sets wrong quantity");
        }

        [Test]
        [TestCase(2, 100)]
        [TestCase(1, 11)]
        [TestCase(5, 0)]
        [TestCase(6, 0)]
        [TestCase(6, 1)]
        [TestCase(7, -1)]
        [TestCase(2, -10)]
        public void SellTest_IncorrectQuantity_ThrowsArgumentOutOfRangeException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testBase.Sell(index, quantity));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Sell should throw {expectedEx.GetType()} if quantity is negative or equal zero");
        }

        [Test]
        [TestCase(-1, 1)]
        [TestCase(8, 1)]
        public void SellTest_IncorrectIndex_ThrowsArgumentOutOfRangeException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testBase.Sell(index, quantity));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Sell should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(0, 5)]
        [TestCase(2, 3)]
        [TestCase(3, 0)]
        [TestCase(1, 10)]
        [TestCase(7, 1)]
        public void AddAtTest_CorrectParameters_SetsQuantityAtIndex(int index, int quantity)
        {
            // Arrange
            var expected = _testArray[index].Item2 + quantity;

            // Act
            _testBase.AddQuantityAt(index, quantity);
            var actual = _testBase[index].quantity;

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Method Sell sets qrong quantity");
        }

        [Test]
        [TestCase(7, -1)]
        [TestCase(2, -10)]
        public void AddAtTest_IncorrectQuantity_ThrowsArgumentOutOfRangeException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testBase.AddQuantityAt(index, quantity));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method AddAt should throw {expectedEx.GetType()} if quantity is negative or equal zero");
        }

        [Test]
        [TestCase(-1, 1)]
        [TestCase(8, 1)]
        public void AddAtTest_IncorrectIndex_ThrowsArgumentOutOfRangeException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testBase.AddQuantityAt(index, quantity));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method AddAt should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        [TestCase(6)]
        [TestCase(7)]
        public void DeleteAtTest_CorrectParameters_DeletesElementAtIndex(int index)
        {
            // Arrange
            var expectedSize = _testArray.Length - 1;
            var expectedArray = new (IProduct, int)[expectedSize];
            int correction = 0;
            for (int i = 0; i < _testArray.Length; i++)
            {
                if (i != index)
                    expectedArray[i - correction] = _testArray[i];
                else
                    correction++;
            }

            // Act
            _testBase.DeleteAt(index);

            // Assert
            Assert.AreEqual(expectedSize, _testBase.Length,
                            message: "Method DeleteAt does not delete the element from list");
            for (int i = 0; i < expectedSize; i++)
            {
                Assert.AreEqual(expectedArray[i], _testBase[i],
                message: $"Method DeleteAt deleted element at another index. Problem at index: {i} (in new list)");
            }
        }

        [Test]
        [TestCase(-1)]
        [TestCase(8)]
        public void DeleteAtTest_IncorrectIndex_ThrowsArgumentOutOfRangeException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testBase.DeleteAt(index));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method DeleteAt should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCaseSource("FindTest_InBase_TestCases")]
        public void FindTest_InBase_ReturnsCorrectIndex(Product product)
        {
            // Arrange
            var expected = -1;
            for (int i = 0; i < _testArray.Length; i++)
            {
                if (_testArray[i].Item1.Equals(product))
                {
                    expected = i;
                    break;
                }
            }

            // Act
            var actual = _testBase.Find(product);

            // Assert
            Assert.AreEqual(expected, actual,
                message: "Method Find returns incorrect index");
        }

        [Test]
        [TestCaseSource("FindTest_NotInBase_TestCases")]
        public void FindTest_NotInBase_ReturnsMinusOne(Product product)
        {
            // Arrange
            const int expected = -1;

            // Act
            var actual = _testBase.Find(product);

            // Assert
            Assert.AreEqual(expected, actual,
                message: "Method Find returns index for a product that was not found");
        }

        [Test]
        [TestCaseSource("AddTest_CorrectProduct_TestCases")]
        public void AddTest_CorrectProduct_AddsProduct(ProductBase productBase, (Product, int) product)
        {
            //Arrange
            int expectedLength = productBase.Length + 1;

            // Act
            productBase.Add(product);

            // Assert
            Assert.AreEqual(expectedLength, productBase.Length, "Method Add does not adds the product");
            Assert.AreEqual(product, productBase[^1], "Method Add does not adds the product");
            Assert.AreSame(product.Item1, productBase[^1].product, "Product and it in order should have the same references");
        }

        [Test]
        [TestCaseSource("AddTest_ExistingProduct_TestCases")]
        public void AddTest_ExistingProduct_IncreasesQuantity(ProductBase productBase, (Product, int) product)
        {
            // Arrange
            int expectedLength = productBase.Length;
            int index = -1;
            for (int i = 0; i < expectedLength; i++)
            {
                if (productBase[i].product.Equals(product.Item1))
                {
                    index = i;
                    break;
                }
            }
            int expectedQuantity = productBase[index].quantity + product.Item2;

            // Act
            productBase.Add(product);

            // Assert
            Assert.AreEqual(expectedLength, productBase.Length, "Method Add should not add new element in case if it already i list. It should just increase the quantity");
            Assert.AreEqual(expectedQuantity, productBase[index].quantity, "Method Add does not increase the quantity");
        }

        [Test]
        [TestCaseSource("AddTest_NullProduct_TestCases")]
        public void AddTest_NullProduct_ThrowsArgumentNullException(ProductBase productBase, (Product, int) product)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => productBase.Add(product));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Add should throw {expectedEx.GetType()} if parameter product.Item1 is null");
        }

        [Test]
        [TestCaseSource("AddTest_IncorrectQuantity_TestCases")]
        public void AddTest_IncorrectQuantity_ThrowsArgumentNullException(ProductBase productBase, (Product, int) product)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => productBase.Add(product));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Add should throw {expectedEx.GetType()} if quantity is negative or equal zero");
        }

        [Test]
        public void EnumeratorTest_ReturnsTupleWithSameReference()
        {
            // Arrange
            List<(IProduct, int)> actualList = new List<(IProduct, int)>();

            // Act
            foreach (var item in _testBase)
                actualList.Add(item);

            // Assert
            for (int i = 0; i < _testArray.Length; i++)
            {
                Assert.AreEqual(_testArray[i], actualList[i], "Enumerator returns incorrect value");
                Assert.AreSame(_testArray[i].Item1, actualList[i].Item1, "Enumerator should return tuple with the same Item1'st reference");
            }
        }
    }
}
