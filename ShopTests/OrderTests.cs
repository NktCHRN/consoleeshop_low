﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ShopLogic;
using ShopLogic.Interfaces;
using Moq;

namespace ShopTests
{
    [TestFixture]
    public class OrderTests
    {

        private static readonly IBuyer _testCustomer = new Mock<IBuyer>().Object;

        private static IDateTimeProvider _dateTimeProvider;

        private static readonly string _testAddress = "temp Address, 1";

        private readonly (IProduct, int)[] _testArray =  { (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)};

        private Order _testOrder;

        [SetUp]
        public void Setup()
        {
            _testOrder = new Order(_testArray, _testCustomer);
            var dateTimeProviderMock = new Mock<IDateTimeProvider>();
            var tempDateTime = new DateTime(2021, 6, 18, 3, 16, 21);
            dateTimeProviderMock.Setup(time => time.Now).Returns(tempDateTime);
            _dateTimeProvider = dateTimeProviderMock.Object;
        }

        private static IEnumerable<TestCaseData> FindTest_InOrder_TestCases
        {
            get
            {
                yield return new TestCaseData(new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m));
                yield return new TestCaseData(new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m));
                yield return new TestCaseData(new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m));
                yield return new TestCaseData(new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m));
            }
        }

        private static IEnumerable<TestCaseData> FindTest_NotInOrder_TestCases
        {
            get
            {
                yield return new TestCaseData(null);
                yield return new TestCaseData(new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m));
            }
        }

        private static IEnumerable<TestCaseData> AddTest_CorrectProduct_TestCases
        {
            get
            {
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 1));
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 5));
                yield return new TestCaseData(new Order(_testCustomer), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 10));
                yield return new TestCaseData(new Order(_testCustomer), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 1));
            }
        }

        
        private static IEnumerable<TestCaseData> AddTest_ExistingProduct_TestCases
        {
            get
            {
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 1));
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 10));
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 3));
            }
        }

        private static IEnumerable<TestCaseData> AddTest_NullProduct_TestCases
        {
            get
            {
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), ((Product)null, 1));
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), ((Product)null, 5));
                yield return new TestCaseData(new Order(_testCustomer), ((Product)null, 10));
                yield return new TestCaseData(new Order(_testCustomer), ((Product)null, 1));
            }
        }

        private static IEnumerable<TestCaseData> AddTest_IncorrectQuantity_TestCases
        {
            get
            {
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), 0));
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2)}, _testCustomer), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), -1));
                yield return new TestCaseData(new Order(_testCustomer), (new Product("Smartphone", Categories.Electronics, "A powerful flagman smartphone", 30000m), -5));
            }
        }

        private static IEnumerable<TestCaseData> CalculatePriceTest_TestCases
        {
            get
            {
                yield return new TestCaseData(new Order(_testCustomer), 0m);
                yield return new TestCaseData(new Order(new (IProduct, int)[] { (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 1) }, _testCustomer), 77.55m);
                yield return new TestCaseData(new Order(new (IProduct, int)[] { (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 2) }, _testCustomer), 155.1m);
                yield return new TestCaseData(new Order(new (IProduct, int)[] { (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 2),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 1)}, _testCustomer), 505.1m);
                yield return new TestCaseData(new Order(new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1)}, _testCustomer), 60051m);
            }
        }


        [Test]
        [TestCase(null)]
        public void IEnumerableConstructorTest_ThrowsAgrumentNullException(IEnumerable<(IProduct, int)> goods)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => new Order(goods, _testCustomer));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Constructor should throw {expectedEx.GetType()} if IEnumerable goods is null");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void IndexerTest_ReturnsTupleWithAnotherProductRef(int index)
        {
            // Arrange
            var expected = _testArray[index];

            // Act
            var actual = _testOrder[index];

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Indexer does not return right value");
            Assert.AreNotSame(expected.Item1, actual.product,
                            message: "Product's references should not be the same");
        }

        [Test]
        [TestCase(-3)]
        [TestCase(-1)]
        [TestCase(5)]
        [TestCase(10)]
        public void IndexerTest_ThrowsArgumentOutOfRangeException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            (IProduct, int) func() => _testOrder[index];
            Exception actualEx = Assert.Catch(() => func());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Indexer should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(0, 1)]
        [TestCase(1, 10)]
        [TestCase(4, 5)]
        public void SetQuantityAtTest_CorrectParameters_ReturnsTrue(int index, int quantity) 
        {
            // Arrange
            var expected = quantity;

            // Act
            _testOrder.SetQuantityAt(index, quantity);
            var actual = _testOrder[index].quantity;

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Method SetQuantity works incorrectly");
        }

        [Test]
        [TestCase(0, 1)]
        [TestCase(1, 10)]
        [TestCase(4, 5)]
        public void SetQuantityAtTest_Confirmed_ThrowsInvalidOperationException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.InvalidOperationException);
            _testOrder.Confirm(_testAddress, _dateTimeProvider);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.SetQuantityAt(index, quantity));


            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method SetQuantity should throw {expectedEx.GetType()} if order is confirmed");
        }

        [Test]
        [TestCase(-1, 1)]
        [TestCase(5, 1)]
        public void SetQuantityAtTest_IncorrectIndex_ThrowsArgumentOutOfRangeException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.SetQuantityAt(index, quantity));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method SetQuantity should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(1, -10)]
        [TestCase(1, -1)]
        [TestCase(1, 0)]
        public void SetQuantityAtTest_IncorrectQuantity_ThrowsArgumentOutOfRangeException(int index, int quantity)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.SetQuantityAt(index, quantity));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method SetQuantity should throw {expectedEx.GetType()} if quantity is negative or equal zero");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void DeleteAtTest_CorrectParameters_ReturnsTrue(int index)
        {
            // Arrange
            var expectedSize = _testArray.Length - 1;
            var expectedArray = new (IProduct, int)[expectedSize];
            int correction = 0;
            for (int i = 0; i < _testArray.Length; i++)
            {
                if (i != index)
                    expectedArray[i - correction] = _testArray[i];
                else
                    correction++;
            }

            // Act
            _testOrder.DeleteAt(index);

            // Assert
            Assert.AreEqual(expectedSize, _testOrder.Length,
                            message: "Method DeleteAt does not delete the element from list");
            for (int i = 0; i < expectedSize; i++)
            {
                Assert.AreEqual(expectedArray[i], _testOrder[i],
                message: $"Method DeleteAt deleted element at another index. Problem at index: {i} (in new list)");
            }
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void DeleteAtTest_Confirmed_ThrowsInvalidOperationException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.InvalidOperationException);
            _testOrder.Confirm(_testAddress, _dateTimeProvider);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.DeleteAt(index));


            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method DeleteAt should throw {expectedEx.GetType()} if order is confirmed");
        }

        [Test]
        [TestCase(-1)]
        [TestCase(5)]
        public void DeleteAtTest_IncorrectIndex_ThrowsArgumentOutOfRangeException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.DeleteAt(index));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method DeleteAt should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        public void Clear_ReturnsTrue()
        {
            // Arrange
            const int expectedSize = 0;

            // Act
            _testOrder.Clear();

            // Assert
            Assert.AreEqual(expectedSize, _testOrder.Length,
                            message: "Method Clear does not clears a list of ordered products");
        }

        [Test]
        public void Clear_Confirmed_ThrowsInvalidOperationException()
        {
            // Arrange
            var expectedEx = typeof(System.InvalidOperationException);
            _testOrder.Confirm(_testAddress, _dateTimeProvider);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.Clear());


            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Clear should throw {expectedEx.GetType()} if order is confirmed");
        }

        [Test]
        [TestCaseSource("FindTest_InOrder_TestCases")]
        public void FindTest_InOrder_ReturnsCorrectIndex(Product product)
        {
            // Arrange
            var expected = -1;
            for (int i = 0; i < _testArray.Length; i++)
            {
                if (_testArray[i].Item1.Equals(product))
                {
                    expected = i;
                    break;
                }
            }

            // Act
            var actual = _testOrder.Find(product);

            // Assert
            Assert.AreEqual(expected, actual,
                message: "Method Find returns incorrect index");
        }

        [Test]
        [TestCaseSource("FindTest_NotInOrder_TestCases")]
        public void FindTest_NotInOrder_ReturnsMinusOne(Product product)
        {
            // Arrange
            const int expected = -1;

            // Act
            var actual = _testOrder.Find(product);

            // Assert
            Assert.AreEqual(expected, actual,
                message: "Method Find returns index for a product that was not found");
        }

        [Test]
        [TestCaseSource("AddTest_CorrectProduct_TestCases")]
        public void AddTest_CorrectProduct_ReturnsTrue(Order order, (Product, int) product)
        {
            //Arrange
            int expectedLength = order.Length + 1;

            // Act
            order.Add(product);

            // Assert
            Assert.AreEqual(expectedLength, order.Length, "Method Add does not adds the product");
            Assert.AreEqual(product, order[^1], "Method Add does not adds the product");
            Assert.AreNotSame(product.Item1, order[^1].product, "Product and it in order should have different references");
        }

        [Test]
        [TestCaseSource("AddTest_ExistingProduct_TestCases")]
        public void AddTest_Confirmed_ThrowsInvalidOperationException(Order order, (Product, int) product)
        {
            // Arrange
            var expectedEx = typeof(System.InvalidOperationException);
            _testOrder.Confirm(_testAddress, _dateTimeProvider);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.Add(product));


            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Add should throw {expectedEx.GetType()} if order is confirmed");
        }

        [Test]
        [TestCaseSource("AddTest_ExistingProduct_TestCases")]
        public void AddTest_ExistingProduct_IncreasesQuantity(Order order, (Product, int) product)
        {
            // Arrange
            int expectedLength = order.Length;
            int index = -1;
            for (int i = 0; i < expectedLength; i++)
            {
                if (order[i].product.Equals(product.Item1))
                {
                    index = i;
                    break;
                }
            }
            int expectedQuantity = order[index].quantity + product.Item2;

            // Act
            order.Add(product);

            // Assert
            Assert.AreEqual(expectedLength, order.Length, "Method Add should not add new element in case if it already i list. It should just increase the quantity");
            Assert.AreEqual(expectedQuantity, order[index].quantity, "Method Add does not increase the quantity");
        }

        [Test]
        [TestCaseSource("AddTest_NullProduct_TestCases")]
        public void AddTest_NullProduct_ThrowsArgumentNullException(Order order, (Product, int) product)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => order.Add(product));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Add should throw {expectedEx.GetType()} if parameter product.Item1 is null");
        }

        [Test]
        [TestCaseSource("AddTest_IncorrectQuantity_TestCases")]
        public void AddTest_IncorrectQuantity_ThrowsArgumentNullException(Order order, (Product, int) product)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => order.Add(product));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Add should throw {expectedEx.GetType()} if quantity is negative or equal zero");
        }

        [Test]
        public void EnumeratorTest_ReturnsTupleWithDifferentReference()
        {
            // Arrange
            List<(IProduct, int)> actualList = new List<(IProduct, int)>();

            // Act
            foreach (var item in _testOrder)
                actualList.Add(item);

            // Assert
            for (int i = 0; i < _testArray.Length; i++)
            {
                Assert.AreEqual(_testArray[i], actualList[i], "Enumerator returns incorrect value");
                Assert.AreNotSame(_testArray[i].Item1, actualList[i].Item1, "Enumerator should return tuple with another Item1'st reference");
            }
        }

        [Test]
        [TestCase("City Kyiv, Shevchenka 1", 2020, 1, 1, 11, 21, 35)]
        public void ConfirmTest_CorrectAddress(string address, int year, int month, int day, int hour, int minute, int second)
        {
            // Arrange
            var expectedDate = new DateTime(year, month, day, hour, minute, second);
            var dateTimeProviderMock = new Mock<IDateTimeProvider>();
            dateTimeProviderMock.Setup(time => time.Now).Returns(expectedDate);

            // Act
            _testOrder.Confirm(address, dateTimeProviderMock.Object);

            // Assert
            Assert.IsTrue(_testOrder.IsConfirmed, "Method Confirm does not sets the property IsConfirmed");
            Assert.AreEqual(address, _testOrder.Address, "Method Confirm does not sets the address");
            Assert.AreEqual(expectedDate, _testOrder.Date, "Method Confirm does not set a date");
        }

        [Test]
        [TestCase("City Kyiv, Shevchenka 1")]
        public void ConfirmTest_Confirmed_ThrowsInvalidOperationException(string address)
        {
            // Arrange
            var expectedEx = typeof(System.InvalidOperationException);
            _testOrder.Confirm(_testAddress, _dateTimeProvider);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.Confirm(address, _dateTimeProvider));


            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Confirm should throw {expectedEx.GetType()} if order is confirmed");
        }

        [Test]
        [TestCase("City Kyiv, Shevchenka 1")]
        public void ConfirmTest_Empty_ThrowsInvalidOperationException(string address)
        {
            // Arrange
            var expectedEx = typeof(System.InvalidOperationException);
            var order = new Order(_testCustomer);

            // Act
            Exception actualEx = Assert.Catch(() => order.Confirm(address, _dateTimeProvider));


            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Confirm should throw {expectedEx.GetType()} if order is empty");
        }

        [Test]
        [TestCase(null)]
        public void ConfirmTest_NullAddress_ThrowsArgumentNullException(string address)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.Confirm(address, _dateTimeProvider));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Confirm should throw {expectedEx.GetType()} if address is null, empty string or contains only white spaces");
        }


        [Test]
        [TestCase("1")]
        [TestCase("a")]
        [TestCase("88")]
        [TestCase("888")]
        [TestCase("8888")]
        [TestCase("88  88")]
        [TestCase("he")]
        [TestCase("hello world")]
        [TestCase("./.,")]
        [TestCase("8,8")]
        [TestCase("Hello, world?!")]
        [TestCase("h8")]
        [TestCase("")]
        [TestCase("    ")]
        public void ConfirmTest_TooSmallAddress_ThrowsFormatException(string address)
        {
            // Arrange
            var expectedEx = typeof(System.FormatException);

            // Act
            Exception actualEx = Assert.Catch(() => _testOrder.Confirm(address, _dateTimeProvider));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Confirm should throw {expectedEx.GetType()} if address is null, empty string or contains only white spaces");
        }

        [Test]
        [TestCaseSource("CalculatePriceTest_TestCases")]
        public void CalculatePriceTests_ReturnsFullPriceForTheOrder(IOrder order, decimal expected)
        {
            // Act
            decimal actual = order.CalculatePrice();

            // Assert
            Assert.AreEqual(expected, actual, "Method CalculatePrice calculates total price for order wrongly");
        }
    }
}
