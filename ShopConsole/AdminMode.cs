﻿using ShopLogic;
using ShopLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShopConsole
{
    public delegate void Operation(ICustomer customer);

    public static class AdminMode
    {
        private static IAdmin _admin;

        private static Operation _operation;

        public static void PrintMenu(IAdmin admin)
        {
            _admin = admin ?? throw new ArgumentNullException(nameof(admin), "Admin cannot be null");
            GuestMode.BuyThingDelegate = GuestMode.PrintProductInfoUnbuyable;
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("[ADMINISTRATOR]");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            PrintHelp();
            bool parsed;
            short choice;
            const short minPoint = 0;
            const short maxPoint = 8;
            do
            {
                Console.WriteLine($"\nEnter the number {minPoint} - {maxPoint}: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed || choice < minPoint || choice > maxPoint)
                {
                    Console.WriteLine($"Error: you entered not a number or number was smaller than {minPoint} or bigger than {maxPoint}.");
                    Console.WriteLine($"Quit - {minPoint}");
                    choice = minPoint - 1;
                }
                switch (choice)
                {
                    case 1:
                        GuestMode.PrintShowCase();
                        break;
                    case 2:
                        StartOrder();
                        break;
                    case 3:
                        PrintInfo();
                        break;
                    case 4:
                        PrintCustomersPrep();
                        break;
                    case 5:
                        AddProduct();
                        break;
                    case 6:
                        PrintFullProductBase();
                        break;
                    case 7:
                        PrintOrders();
                        break;
                }
                if (choice > minPoint && choice <= maxPoint)
                {
                    Program.PrintHeader();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("[ADMINISTRATOR]");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    PrintHelp();
                }
            } while (choice != minPoint);
        }

        public static void PrintSelected(ICustomer customer)
        {
            CustomerMode.Person = customer;
            CustomerMode.PersonAuthorizable = customer;
            CustomerMode.Buyer = customer;
            CustomerMode.PrintInfo();
        }

        public static void PrintCustomersPrep()
        {
            _operation = PrintSelected;
            PrintCustomers();
        }

        public static void PrintCustomers()
        {
            const short idLength = -4;
            const short fullNameLength = -30;
            short choice;
            bool parsed;
            Program.SetSize();
            do
            {
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"{"ID",idLength}{"Customer's full name",fullNameLength}");
                foreach (ICustomer customer in GuestMode.Shop.Customers)
                    Console.WriteLine($"{customer.ID,idLength}{customer.GetFullName(),fullNameLength}");
                Console.WriteLine("\nSelect the customer by ID (0 - quit)");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                {
                    choice = -1;
                }
                else if (choice != 0)
                {
                    foreach (ICustomer customer in GuestMode.Shop.Customers)
                    {
                        if (customer.ID == choice)
                        {
                            _operation?.Invoke(customer);
                            break;
                        }
                    }
                }
            } while (choice != 0);
        }

        public static bool SetLogin()
        {
            bool firstTime = true;
            bool rerun;
            string login;
            Console.WriteLine("\nEnter the login:");
            do
            {
                rerun = false;
                login = Console.ReadLine();
                if (login == "0" && !firstTime)
                {
                    rerun = false;
                }
                else if (!GuestMode.Shop.IsFreeLogin(login))
                {
                    Console.WriteLine("You phone number is already registered");
                    Console.WriteLine("Enter the login once more or 0 to cancel the operation:");
                    rerun = true;
                }
                else
                {
                    try
                    {
                        _admin.Login = login;
                    }
                    catch (FormatException exc)
                    {
                        Console.WriteLine(exc.Message);
                        Console.WriteLine("Enter the login once more or 0 to cancel the operation:");
                        rerun = true;
                    }
                }
                if (firstTime)
                    firstTime = false;
            } while (rerun);
            return login != "0";
        }

        public static void ChangeLogin()
        {
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Before changing the login, you need to confirm your identity first");
            if (GuestMode.CheckPassword(_admin))
            {
                Console.WriteLine("Now, you can change the login");
                if (SetLogin())
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("\nCongratulations!");
                    Console.WriteLine($"You successfully changed the login to {_admin.Login}");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("\nPress [ENTER] to continue");
                    Console.ReadLine();
                }
            }
        }

        public static void ChangePassword()
        {
            CustomerMode.PersonAuthorizable = _admin;
            CustomerMode.ChangePassword(true);
        }

        public static void PrintInfo()
        {
            short choice;
            bool parsed;
            const short minPoint = 0;
            const short maxPoint = 2;
            Program.SetSize();
            do
            {
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Info about an admin:");
                Console.WriteLine($"Login: {_admin.Login}");
                Console.WriteLine();
                Console.WriteLine("1. Change the login");
                Console.WriteLine("2. Change the password");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("0. Go back to menu");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"\nEnter the number {minPoint} - {maxPoint}: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                    choice = minPoint - 1;
                switch (choice)
                {
                    case 1:
                        ChangeLogin();
                        break;
                    case 2:
                        ChangePassword();
                        break;
                }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
            } while (choice != 0);
        }

        private static string EnterProductName()
        {
            bool firstTime = true;
            Console.WriteLine("Enter the name of the product:");
            string name = Console.ReadLine();
            while ((string.IsNullOrWhiteSpace(name) || name.Length < 2) && (name != "0" || firstTime))
            {
                Console.WriteLine("Name cannot be empty, have length of 1, be 0, or contain only white spaces");
                Console.WriteLine("Enter the name of a product (0 - cancel):");
                name = Console.ReadLine();
                if (firstTime)
                    firstTime = false;
            }
            return name;
        }

        private static short EnterCategory()
        {
            const short categoriesQuantity = 10;
            bool parsed;
            Console.WriteLine("\nAvailable categories: ");
            for (int i = 1; i <= categoriesQuantity; i++)
                Console.WriteLine($"{i} - {(Categories)(i - 1)}");
            bool firstTime = true;
            Console.WriteLine("Enter the category of the product");
            parsed = short.TryParse(Console.ReadLine(), out short entered);
            while ((!parsed || entered <= 0 || entered > categoriesQuantity) && (entered != 0 || firstTime))
            {
                Console.WriteLine("You have entered the wrong category");
                Console.WriteLine("Enter the category of the product (0 - cancel):");
                parsed = short.TryParse(Console.ReadLine(), out entered);
                if (firstTime)
                    firstTime = false;
            }
            return (short)(entered - 1);
        }

        private static string EnterDescription()
        {
            bool firstTime = true;
            Console.WriteLine("\nEnter the description of the product:");
            string description = Console.ReadLine();
            while ((string.IsNullOrWhiteSpace(description) || description.Length < 2) && (description != "0" || firstTime))
            {
                Console.WriteLine("Description cannot be empty, have length of 1, be 0, or contain only white spaces");
                Console.WriteLine("Enter the description of a product (0 - cancel):");
                description = Console.ReadLine();
                if (firstTime)
                    firstTime = false;
            }
            return description;
        }

        private static decimal EnterPrice()
        {
            bool firstTime = true;
            Console.WriteLine("\nEnter the price of the product:");
            bool parsed = decimal.TryParse(Console.ReadLine().Replace('.', ','), out decimal price);
            while ((!parsed || price <= 0) && (price != 0 || firstTime))
            {
                Console.WriteLine("You have entered the wrong price");
                Console.WriteLine("Enter the price of the product once more (0 - cancel):");
                parsed = decimal.TryParse(Console.ReadLine().Replace('.', ','), out price);
                if (firstTime)
                    firstTime = false;
            }
            return price;
        }

        private static int EnterQuantity()
        {
            Console.WriteLine("\nEnter the quantity of the product: ");
            bool parsed = int.TryParse(Console.ReadLine(), out int quantity);
            while (!parsed || quantity < 0)
            {
                Console.WriteLine("You have entered the wrong (negative) quantity");
                Console.WriteLine("Enter the quantity of the product once more: ");
                parsed = int.TryParse(Console.ReadLine(), out quantity);
            }
            return quantity;
        }

        public static void AddProduct()
        {
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string name = EnterProductName();
            if (name == "0")
                return;
            short entered = EnterCategory();
            if (entered == -1)
                return;
            Categories category = (Categories)entered;
            string description = EnterDescription();
            if (description == "0")
                return;
            decimal price = EnterPrice();
            if (price == 0m)
                return;
            int quantity = EnterQuantity();
            IProduct newProduct = new Product(name, category, description, price);
            GuestMode.Shop.ShowCase.Add((newProduct, quantity));
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine("You successfully added the product");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void PrintFullProductBase()
        {
            const short halfCategories = 5;
            const short totallyCategories = 10;
            const short sortsQuantity = 2;
            const short numberLength = -4;
            const short nameMaxLength = -20;
            const short categoryMaxLength = -20;
            const short priceMaxLength = -14;
            const short quantityMaxLength = -10;
            string find = string.Empty;
            bool findMode = false;
            short filter = -1;
            short sortBy = 0;
            string entered;
            short temp;
            IGettableProductCollection productBase;
            List<(IProduct, int)> products;
            do
            {
                productBase = GuestMode.Shop.ShowCase;
                products = GuestMode.GetSorted(productBase, (ProductSortingTypes)sortBy).ToList();
                if (filter > -1)
                    products = GuestMode.GetFiltered(products, (Categories)filter).ToList();
                if (findMode)
                    products = GuestMode.Find(products, find).ToList();
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Products available:");
                Console.WriteLine($"Filter: f{filter + 1}; Sort by s{sortBy + 1}; FindMode: {(findMode ? "On" : "Off")}");
                Console.Write("[Filters: f0 - All; ");
                for (int i = 1; i <= halfCategories; i++)
                    Console.Write($"f{i} - {(Categories)(i - 1)}; ");
                Console.WriteLine();
                for (int i = halfCategories; i <= totallyCategories; i++)
                    Console.Write($"f{i} - {(Categories)(i - 1)}; ");
                Console.WriteLine("]");

                Console.Write("[Sorting types: ");
                for (int i = 1; i <= sortsQuantity; i++)
                    Console.Write($"s{i} - {(ProductSortingTypes)(i - 1)}; ");
                Console.WriteLine("]");
                Console.WriteLine("[To find a product, enter: find name]");
                Console.WriteLine("[To stop searching and get back to full menu: stopfind]");
                Console.WriteLine("[To view precise description, change or delete any item, just enter it's number (ex., 1)]");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("[0 - quit]");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"\n{"№",numberLength}{"Name",nameMaxLength}{"Category",categoryMaxLength}{"Price",priceMaxLength}{"Quantity", quantityMaxLength}");
                for (int i = 0; i < products.Count; i++)
                    Console.WriteLine($"{i + 1,numberLength}{products[i].Item1.Name,nameMaxLength}{products[i].Item1.Category,categoryMaxLength}{string.Format("{0:F2}", products[i].Item1.Price) + " UAH",priceMaxLength}{products[i].Item2, quantityMaxLength}");
                Console.WriteLine("\nEnter any filter, sorting type or number of product you want to view description of or change. 0 - quit");
                entered = Console.ReadLine().Trim().ToLower();
                if (entered.Length > 0)
                {
                    if (char.IsLetter(entered[0]) && entered.Length > 1)
                    {
                        if (entered[0] == 'f')
                        {
                            if (char.IsDigit(entered[1]))
                            {
                                short.TryParse(entered.Remove(0, 1), out temp);
                                if (temp >= 0 && temp <= totallyCategories)
                                {
                                    temp--;
                                    filter = temp;
                                }
                            }
                            else if (entered.StartsWith("find "))
                            {
                                find = entered.Remove(0, 5);
                                findMode = true;
                            }
                        }
                        else if (entered[0] == 's')
                        {
                            if (char.IsDigit(entered[1]))
                            {
                                short.TryParse(entered.Remove(0, 1), out temp);
                                if (temp > 0 && temp <= sortsQuantity)
                                {
                                    temp--;
                                    sortBy = temp;
                                }
                            }
                            else if (entered.StartsWith("stopfind"))
                            {
                                findMode = false;
                            }
                        }
                    }
                    else
                    {
                        int.TryParse(entered, out int enteredProductNumber);
                        enteredProductNumber--;
                        if (enteredProductNumber >= 0 && enteredProductNumber < products.Count)
                            ChangeProduct(products[enteredProductNumber].Item1, products[enteredProductNumber].Item2);
                    }
                }
            } while (entered != "0");
            Console.ResetColor();
        }

        public static void ChangeProductName(IProduct product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string newName = EnterProductName();
            if (newName == "0")
                return;
            product.Name = newName;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully changed the product name to {product.Name}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void ChangeCategory(IProduct product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            short temp = EnterCategory();
            if (temp == -1)
                return;
            Categories newCategory = (Categories)temp;
            product.Category = newCategory;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully changed the product category to {product.Category}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void ChangeDescription(IProduct product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string newDescription = EnterDescription();
            if (newDescription == "0")
                return;
            product.Description = newDescription;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully changed the product description to \n{product.Description}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void ChangePrice(IProduct product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            decimal newPrice = EnterPrice();
            if (newPrice == 0m)
                return;
            product.Price = newPrice;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully changed the product price to {product.Price} UAH");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void ChangeQuantity(IProduct product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            int quantity = EnterQuantity();
            GuestMode.Shop.ShowCase.SetQuantityAt(GuestMode.Shop.ShowCase.Find(product), quantity);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully changed the product quantity to {quantity}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static bool DeleteProduct(IProduct product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string entered;
            Console.WriteLine($"\nDo you want really to delete the product \"{product.Name}\"? [Y/n]");
            entered = Console.ReadLine().Trim().ToLower() + " ";
            while (entered[0] != 'n' && entered[0] != 'y')
            {
                Console.WriteLine("Error. Please, enter Y on n once more");
                Console.WriteLine($"\nDo you really want to delete the product \"{product.Name}\"? [Y/n]");
                entered = Console.ReadLine().Trim().ToLower() + " ";
            }
            if (entered[0] == 'n')
                return false;
            GuestMode.Shop.ShowCase.DeleteAt(GuestMode.Shop.ShowCase.Find(product));
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully deleted the product");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
            return true;
        }

        private static void ChangeProduct(IProduct product, int quantity)
        {
            short choice;
            bool parsed;
            const short minPoint = 0;
            const short maxPoint = 6;
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            do
            {
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Product info");
                Console.WriteLine($"Name: {product.Name}");
                Console.WriteLine($"Category: {product.Category}");
                Console.WriteLine($"Description: {product.Description}");
                Console.WriteLine($"Price: {string.Format("{0:F2}", product.Price)} UAH");
                Console.WriteLine($"Quantity: {quantity}");
                Console.WriteLine("\nWhat do you want to do?");
                Console.WriteLine("1. Change name of the product");
                Console.WriteLine("2. Change category of the product");
                Console.WriteLine("3. Change description of the product");
                Console.WriteLine("4. Change price of the product");
                Console.WriteLine("5. Change quantity");
                Console.WriteLine("6. Delete the product");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("0. Go back to product base");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"\nEnter the number {minPoint} - {maxPoint}: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                    choice = minPoint - 1;
                switch (choice)
                {
                    case 1:
                        ChangeProductName(product);
                        break;
                    case 2:
                        ChangeCategory(product);
                        break;
                    case 3:
                        ChangeDescription(product);
                        break;
                    case 4:
                        ChangePrice(product);
                        break;
                    case 5:
                        ChangeQuantity(product);
                        break;
                    case 6:
                        bool deleted = DeleteProduct(product);
                        if (deleted)
                            choice = 0;
                        break;
                }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
            } while (choice != 0);
        }

        public static void PrintOrders()
        {
            short choice;
            bool parsed;
            const short idLength = -4;
            const short customerMaxLength = -30;
            const short timeMaxLength = -20;
            const short productMaxLength = -30;
            const short statusMaxLength = -18;
            const short totalPriceMaxLength = -15;
            Program.SetSize();
            IEnumerable<IOrder> orders;
            do
            {
                orders = GuestMode.Shop.GetAllOrders();
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Orders:");
                Console.WriteLine($"\n{"ID",idLength}{"Customer",customerMaxLength}{"Date and time",timeMaxLength}{"Products",productMaxLength}{"Status",statusMaxLength}{"Total price",totalPriceMaxLength}");
                foreach (var order in orders)
                    CustomerMode.PrintShortOrderInfo(order, false);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nEnter the ID to show information about, 0 - quit: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                    choice = -1;
                foreach (var order in orders)
                {
                    if (order.ID == choice)
                    {
                        ChangeStatus(order);
                        break;
                    }
                }
            } while (choice != 0);
        }

        public static void ChangeStatus(IOrder order)
        {
            bool parsed;
            const short statusQuantity = 6;
            CustomerMode.PrintOrder(order);
            Console.WriteLine("\nStatuses:");
            Console.WriteLine("1. New");
            Console.WriteLine("2. Cancelled by admin");
            Console.WriteLine("3. Payment received");
            Console.WriteLine("4. Sent");
            Console.WriteLine("5. Received");
            Console.WriteLine("6. Ended");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("0. Quit");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nEnter the new status of the order:");
            parsed = short.TryParse(Console.ReadLine(), out short newStatus);
            while (!parsed || newStatus < 0 || newStatus > statusQuantity)
            {
                Console.WriteLine("You have entered the wrong status number");
                Console.WriteLine("Enter the new status of the order once more (0 - quit):");
                parsed = short.TryParse(Console.ReadLine(), out newStatus);
            }
            if (newStatus == 0)
                return;
            if (newStatus <= 2)
                newStatus--;
            OrderStatuses oldStatus = order.Status;
            _admin.SetStatus(order, (OrderStatuses)newStatus);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully changed the status of order {order.ID} from {oldStatus} to {order.Status}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void StartOrder()
        {
            const int adminID = -1;
            const short idLength = -4;
            const short fullNameLength = -30;
            short choice;
            bool parsed;
            CustomerMode.Buyer = null;
            Program.SetSize();
            do
            {
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"{"ID",idLength}{"Full name",fullNameLength}");
                Console.WriteLine("Admin (me):");
                Console.WriteLine($"{-1,idLength}{_admin,fullNameLength}");
                Console.WriteLine("Customers:");
                foreach (ICustomer customer in GuestMode.Shop.Customers)
                    Console.WriteLine($"{customer.ID,idLength}{customer.GetFullName(),fullNameLength}");
                Console.WriteLine("\nSelect the user to issue order on by ID (0 - cancel):");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                {
                    choice = adminID - 1;
                }
                else if (choice != 0)
                {

                    if (choice == adminID)
                    {
                        CustomerMode.Buyer = _admin;
                    }
                    else
                    {
                        foreach (ICustomer customer in GuestMode.Shop.Customers)
                        {
                            if (customer.ID == choice)
                            {
                                CustomerMode.Buyer = customer;
                                break;
                            }
                        }
                    }
                    if (CustomerMode.Buyer != null)
                    {
                        CustomerMode.Cart = new Order(CustomerMode.Buyer);
                        CustomerMode.PrintCart();
                        CustomerMode.ClearCart();
                        CustomerMode.Buyer = null;
                        choice = 0;
                    }
                }
            } while (choice != 0);
        }

        public static void PrintHelp()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Menu: ");
            Console.WriteLine("1. View goods on sale");     // unbuyable menu
            Console.WriteLine("2. Start an order");         // from customer menu, on admin or one of the customers
            Console.WriteLine("3. View or edit admin information");  // + change login, password
            Console.WriteLine("4. View or edit information about a customer");
            Console.WriteLine("5. Add product");
            Console.WriteLine("6. View all products in base");      // + change info about a product, quantity, delete product
            Console.WriteLine("7. View all orders");                // + change order's status
            Console.WriteLine("8. Help");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("0. Log out");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
        }
    }
}
