﻿using ShopLogic.Interfaces;
using ShopLogic.HelperClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ShopLogic
{
    public class Customer : RegisteredUser, ICustomer
    {

        private readonly List<IOrder> _orders;

        private readonly List<(object sender, StatusEventArgs eventArgs)> _unreadNotifications;

        public IReadOnlyList<(object sender, StatusEventArgs eventArgs)> UnreadNotifications => _unreadNotifications;

        private static int _customersQuantity = 0;

        /// <summary>
        /// Total quantity of customers
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when customer quantity now is not 0 or given value is negative or equal 0 (in setter)</exception>
        public static int CustomersQuantity
        {
            get
            {
                return _customersQuantity;
            }
            set
            {
                if (value >= _customersQuantity && _customersQuantity == 0)
                {
                    _customersQuantity = value;
                }
                else
                {
                    throw new InvalidOperationException("CustomerQuantity may be setted only on creation of the project");
                }
            }
        }

        private string _firstName;

        public string FirstName 
        { 
            get
            {
                return _firstName;
            } 
            set 
            {
                if (value.IsValidName())
                    _firstName = value;
                else
                    throw new FormatException("Wrong first name format (at least two letters, first is capital)");
            } 
        }

        private string _lastName;

        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                if (value.IsValidName())
                    _lastName = value;
                else
                    throw new FormatException("Wrong first name format (at least two letters, first is capital)");
            }
        }

        [DataType(DataType.Date)]
        private DateTime _birthday;

        [DataType(DataType.Date)]
        public DateTime Birthday 
        { 
            get
            {
                return _birthday;
            }
            set
            {
                const int minBirthYear = 1870;
                DateTime maxBirthDay = DateTime.Now;
                if (value.Year >= minBirthYear && value <= maxBirthDay)
                    _birthday = value;
                else
                    throw new ArgumentOutOfRangeException(nameof(value), $"BirthDay year cannot be less than {minBirthYear}");
            }
        }

        private string _phoneNumber;

        public string PhoneNumber
        {
            get
            {
                return _phoneNumber;
            }
            set
            {
                if (value.IsValidPhoneNumber())
                    _phoneNumber = value.NormalizePhoneNumber();
                else
                    throw new FormatException("Wrong phone number format");
            }
        }

        private string _email;

        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                if (value.IsValidEmail())
                    _email = value;
                else
                    throw new FormatException("Wrong email format");
            }
        }

        /// <summary>
        /// Automatically generated ID
        /// </summary>
        public int ID { get; private set; }

        public int OrdersQuantity => _orders.Count;

        public IOrder this[int index] 
        { 
            get
            {
                if (index < 0 || index >= _orders.Count)
                    throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than size of list of orders");
                return _orders[index];
            }
        }

        public Customer(string firstName, string lastName, DateTime birthDay, string email, string phoneNumber, string password, INotifier statusSetNotifier) : base(password)
        {
            FirstName = firstName;
            LastName = lastName;
            Birthday = birthDay;
            Email = email;
            PhoneNumber = phoneNumber;
            _orders = new List<IOrder>();
            _unreadNotifications = new List<(object sender, StatusEventArgs eventArgs)>();
            _customersQuantity++;
            ID = _customersQuantity;
            statusSetNotifier.StatusSetEvent += StatusSetHandler;
        }

        public override bool CheckLogin(string toCheck)
        {
            if (toCheck == null)
                throw new ArgumentNullException(nameof(toCheck), "Potential login cannot be null");
            return toCheck.NormalizePhoneNumber() == _phoneNumber || toCheck == _email;
        }

        
        public string GetFullName() => $"{FirstName} {LastName}";

        public void AddOrder(IOrder order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order), "Order cannot be null");
            if (!order.IsConfirmed)
                throw new ArgumentException("Order to add to the history should be confirmed first", nameof(order));
            if (order.Customer.ID != ID)
                throw new ArgumentException("Order to add to the history should have the same customer", nameof(order));
            _orders.Add(order);
        }

        public void ClearNotifications()
        {
            _unreadNotifications.Clear();
        }

        public void StatusSetHandler(object sender, StatusEventArgs e)
        {
            if (sender == null)
                throw new ArgumentNullException(nameof(sender), "Sender cannot be null");
            if (e == null)
                throw new ArgumentNullException(nameof(e), "Event arguments cannot be null");
            if (e.Order.Customer.ID == ID)
                _unreadNotifications.Add((sender, e));
        }

        public override string ToString() => GetFullName();

        public IEnumerator<IOrder> GetEnumerator() => _orders.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
