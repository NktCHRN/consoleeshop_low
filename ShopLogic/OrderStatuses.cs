﻿namespace ShopLogic
{
    public enum OrderStatuses
    {
        New,
        CancelledByAdmin,
        CancelledByUser,
        PaymentReceived,
        Sent,
        Received,
        Ended
    }
}
