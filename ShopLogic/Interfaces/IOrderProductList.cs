﻿namespace ShopLogic.Interfaces
{
    public interface IOrderProductList : IGettableProductCollection
    {

        /// <summary>
        /// Enambles and disables operations Confirm, SetQuantityAt, DeleteAt, Add, Clear
        /// </summary>
        public bool IsConfirmed { get; }

        public void Confirm(string address, IDateTimeProvider timeProvider);

        public void SetQuantityAt(int index, int quantity);

        public void DeleteAt(int index);

        public void Add((IProduct product, int quantity) item);

        public void Clear();
    }
}
