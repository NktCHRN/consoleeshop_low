﻿namespace ShopLogic.Interfaces
{
    public interface INonCustomerStatusSetter
    {
        public void SetStatus(IOrderDetails order, OrderStatuses newStatus);
    }
}
