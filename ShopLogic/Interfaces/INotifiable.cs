﻿using System.Collections.Generic;

namespace ShopLogic.Interfaces
{
    public interface INotifiable
    {
        public IReadOnlyList<(object sender, StatusEventArgs eventArgs)> UnreadNotifications { get; }

        public void ClearNotifications();

        public void StatusSetHandler(object sender, StatusEventArgs e);
    }
}
