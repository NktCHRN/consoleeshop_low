﻿namespace ShopLogic.Interfaces
{
    public interface IProductBase : IGettableProductCollection
    {
        public void SetQuantityAt(int index, int quantity);

        public void DeleteAt(int index);

        public void Add((IProduct product, int quantity) item);

        public (IProduct product, int quantity) Sell(int index, int quantity);

        public void AddQuantityAt(int index, int quantity);
    }
}
