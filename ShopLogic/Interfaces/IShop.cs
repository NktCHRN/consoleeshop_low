﻿using ShopLogic.HelperClasses;

namespace ShopLogic.Interfaces
{
    public interface IShop
    {
        public IAdmin Admin { get; }

        public IProductBase ShowCase { get; }

        public CustomList<ICustomer> Customers { get; }

        public CustomList<IOrder> GetAllOrders();

        public bool IsFreeLogin(string login);
    }
}
