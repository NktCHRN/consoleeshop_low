﻿using ShopLogic.Interfaces;
using System;

namespace ShopLogic
{
    public class StatusEventArgs : EventArgs
    {
        public IOrderDetails Order { get; private set; }

        public OrderStatuses LastStatus { get; private set; }

        public OrderStatuses NewStatus { get; private set; }

        public StatusEventArgs(IOrderDetails order, OrderStatuses oldStatus, OrderStatuses newStatus)
        {
            Order = order ?? throw new ArgumentNullException(nameof(order), "Order cannot be null");
            LastStatus = oldStatus;
            NewStatus = newStatus;
        }
    }
}
