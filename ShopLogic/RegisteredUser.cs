﻿using ShopLogic.HelperClasses;
using ShopLogic.Interfaces;
using System;
using System.Security.Cryptography;
using System.Text;

namespace ShopLogic
{
    public abstract class RegisteredUser : IAuthorizable
    {

        private string _salt;

        internal string Salt
        {
            get => _salt;
            set
            {
                _salt = value ?? throw new ArgumentNullException(nameof(value), "Salt cannot be null");
            }
        }

        private string _hashPassword;

        internal string HashPassword
        {
            get => _hashPassword;
            set
            {
                _hashPassword = value ?? throw new ArgumentNullException(nameof(value), "Hashed password cannot be null");
            }
        }

        /// <summary>
        /// Password property (only setter)
        /// </summary>
        /// <exception cref="ArgumentException">Thrown when value contains whitespaces or smaller than 8 characters</exception>
        /// <exception cref="ArgumentNullException">Thrown when value is null</exception>
        public string Password
        {
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Password can`t be null", nameof(value));
                if (!value.IsValidPassword())
                    throw new FormatException($"Password should have at least 8 characters and should not contain whitespaces and should contain at least one uppercase letter, one lowercase letter and one digit");
                _salt = GenerateSalt();
                _hashPassword = GetHash(value);
            }
        }

        public RegisteredUser(string password)
        {
            Password = password;
        }

        private string GenerateSalt()
        {
            Random random = new Random();
            const int minSize = 4;
            const int maxSize = 16;
            int size = random.Next(minSize, maxSize);
            char[] charArray = new char[size];
            for (int i = 0; i < size; i++)
                charArray[i] = (char)random.Next(0, char.MaxValue);
            return new string(charArray);
        }

        public abstract bool CheckLogin(string toCheck);

        public bool CheckPassword(string toCheck)
        {
            if (toCheck != null)
                return _hashPassword == GetHash(toCheck);
            else
                throw new ArgumentNullException(nameof(toCheck), "Possible password can`t be null");
        }

        private string GetHash(string toHash)
        {
            SHA512 sha512 = SHA512.Create();
            string hashPassword = toHash;
            byte[] hash;
            int times = (int)Math.Pow(2, 8);
            for (int i = 0; i < times; i++)
            {
                hashPassword += _salt;
                hash = sha512.ComputeHash(Encoding.UTF8.GetBytes(hashPassword));
                hashPassword = Convert.ToBase64String(hash);
            }
            return hashPassword;
        }
    }
}
