﻿using ShopLogic.Interfaces;
using ShopLogic.HelperClasses;
using System;
using System.Linq;
using System.Collections.Generic;

namespace ShopLogic
{
    public class Shop : IShop
    {
        public IAdmin Admin { get; private set; }

        public IProductBase ShowCase { get; private set; }

        public CustomList<ICustomer> Customers { get; private set; }

        public Shop(IAdmin admin, IEnumerable<ICustomer> customers, IProductBase productBase)
        {
            Admin = admin ?? throw new ArgumentNullException(nameof(admin), "Admin cannot be null");
            if (customers == null)
                throw new ArgumentNullException(nameof(customers), "Customers cannot be null");
            Customers = new CustomList<ICustomer>(customers);
            ShowCase = productBase ?? throw new ArgumentNullException(nameof(productBase), "ProductBase cannot be null");
        }

        public CustomList<IOrder> GetAllOrders()
        {
            CustomList<IOrder> orders = new CustomList<IOrder>();
            foreach (ICustomer customer in Customers)
                foreach (IOrder order in customer)
                    orders.Add(order);
            foreach (IOrder order in Admin)
                orders.Add(order);
            orders = new CustomList<IOrder>(from order in orders
                         orderby order.ID descending
                         select order);
            return orders;
        }

        public bool IsFreeLogin(string login)
        {
            if (login == null)
                throw new ArgumentNullException(nameof(login), "Login cannot be null");
            foreach (IAuthorizable customer in Customers)
                if (customer.CheckLogin(login))
                    return false;
            return login != Admin.Login;
        }
    }
}
