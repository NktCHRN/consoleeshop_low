﻿using ShopLogic.Interfaces;
using System;

namespace ShopLogic
{
    public class Product : IProduct
    {

        private string _name;

        public string Name 
        { 
            get
            {
                return _name;
            } 
            set 
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException(nameof(value), "Name of the product cannot be null or empty or contain only white spaces");
                _name = value;
            }
        }

        public Categories Category { get; set; }

        private string _description;

        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException(nameof(value), "Description cannot be null or empty or contain only white spaces");
                _description = value;
            }
        }

        private decimal _price;

        public decimal Price
        {
            get
            {
                return _price;
            }
            set
            {
                decimal roundedValue = Math.Round(value * 100m) / 100m;
                if (roundedValue <= 0)
                    throw new ArgumentOutOfRangeException(nameof(value), "Price cannot be negative or equal 0");
                _price = roundedValue;
            }
        }

        public Product(string name, Categories category, string description, decimal price)
        {
            Name = name;
            Category = category;
            Description = description;
            Price = price;
        }

        public object Clone() => new Product(Name, Category, Description, Price);

        public override int GetHashCode() => HashCode.Combine(Name, Category, Price, Description);

        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is Product other))
                return false;
            return GetHashCode() == other.GetHashCode();
        }
    }
}
