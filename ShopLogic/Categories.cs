﻿namespace ShopLogic
{
    public enum Categories
    {
        Electronics,
        SportEquipment,
        Stationery,
        Toys,
        Food,
        Appliances,
        Clothes,
        Jewelry,
        Hygiene,
        HouseholdChemicals
    }
}
