﻿using ShopLogic.Interfaces;
using System;

namespace ShopLogic.HelperClasses
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime Now => DateTime.Now;
    }
}
