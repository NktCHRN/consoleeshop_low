﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ShopLogic.HelperClasses
{
    public class CustomList<T> : IEnumerable<T>
    {
        private List<T> _items;

        public CustomList()
        {
            _items = new List<T>();
        }

        public CustomList(IEnumerable<T> items) : this()
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items), "IEnumerable of items cannot be null");
            foreach (var item in items)
                Add(item);
        }

        public int Count => _items.Count;

        public T this[int index]
        {
            get
            {
                if (index < 0 || index >= _items.Count)
                    throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than list of items size");
                return _items[index];
            }
            set
            {
                if (index < 0 || index >= _items.Count)
                    throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than list of items size");
                if (value == null)
                    throw new ArgumentNullException(nameof(value), "item cannot be null");
                _items[index] = value;
            }
        }

        public void DeleteAt(int index)
        {
            if (index < 0 || index >= _items.Count)
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than list of goods size");
            T temp;
            for (int i = index; i < _items.Count - 1; i++)
            {
                temp = _items[i];
                _items[i] = _items[i + 1];
                _items[i + 1] = temp;
            }
            List<T> newItems = new List<T>();
            for (int i = 0; i < _items.Count - 1; i++)
                newItems.Add(_items[i]);
            _items = newItems;
        }

        public int Find(T item)
        {
            for (int i = 0; i < _items.Count; i++)
                if (_items[i].Equals(item))
                    return i;
            return -1;
        }

        public void Add(T item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Any item cannot be null");
            _items.Add(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in _items)
                yield return item;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
