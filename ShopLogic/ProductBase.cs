﻿using ShopLogic.Interfaces;
using ShopLogic.HelperClasses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ShopLogic
{
    public class ProductBase : IProductBase
    {

        private readonly CustomList<(IProduct product, int quantity)> _products;

        public ProductBase()
        {
            _products = new CustomList<(IProduct product, int quantity)>();
        }

        public ProductBase(IEnumerable<(IProduct product, int quantity)> products) : this()
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products), "IEnumerable of products cannot be null");
            foreach (var item in products)
                Add(item);
        }

        public (IProduct product, int quantity) this[int index] => _products[index];

        public int Length => _products.Count;

        public int Find(IProduct product)
        {
            for (int i = 0; i < _products.Count; i++)
            {
                if (_products[i].product.Equals(product))
                    return i;
            }
            return -1;
        }

        public void Add((IProduct product, int quantity) item)
        {
            if (item.product == null)
            {
                throw new ArgumentNullException(nameof(item.product), "Any item cannot be null");
            }
            else if (item.quantity < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(item.quantity), "Quantity of any product cannot be negative");
            }
            else if (Find(item.product) != -1)
            {
                int index = Find(item.product);
                SetQuantityAt(index, _products[index].quantity + item.quantity);
            }
            else
            {
                _products.Add(item);
            }
        }

        public void DeleteAt(int index) => _products.DeleteAt(index);

        public void SetQuantityAt(int index, int quantity)
        {
            if (index < 0 || index >= _products.Count)
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than size of list of goods");
            else if (quantity < 0)
                throw new ArgumentOutOfRangeException(nameof(quantity), "Quantity of any product cannot be negative");
            else
                _products[index] = (_products[index].product, quantity);
        }

        public (IProduct product, int quantity) Sell(int index, int quantity)
        {
            if (index < 0 || index >= _products.Count)
            {
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than size of list of goods");
            }
            else if (quantity <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(quantity), "Quantity of any product cannot be negative or equal 0");
            }
            else
            {
                SetQuantityAt(index, _products[index].quantity - quantity);
                return (_products[index].product, quantity);
            }
        }

        public void AddQuantityAt(int index, int quantity)
        {
            if (index < 0 || index >= _products.Count)
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than size of list of goods");
            else if (quantity < 0)
                throw new ArgumentOutOfRangeException(nameof(quantity), "Quantity of any product cannot be negative");
            else
                SetQuantityAt(index, _products[index].quantity + quantity);
        }

        public IEnumerator<(IProduct product, int quantity)> GetEnumerator() => _products.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
